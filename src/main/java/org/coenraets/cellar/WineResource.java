package org.coenraets.cellar;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/wines")
public class WineResource {

	WineDAO dao = new WineDAO();
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<Wine> findAll() {
		System.out.println("findAll");
		Response.ok()
			.header("Access-Control-Allow-Origin", "http://localhost:8080")
			.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
			.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
		return dao.findAll();
	}

	@GET @Path("search/{query}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<Wine> findByName(@PathParam("query") String query) {
		System.out.println("findByName: " + query);
		Response.ok()
		.header("Access-Control-Allow-Origin", "http://localhost:8080")
		.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
		.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();
		return dao.findByName(query);
	}

	@GET @Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Wine findById(@PathParam("id") String id) {
		System.out.println("findById " + id);
		Response.ok()
		.header("Access-Control-Allow-Origin", "http://localhost:8080")
		.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, OPTIONS")
		.header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With").build();		
		return dao.findById(Integer.parseInt(id));
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Wine create(Wine wine) {
		return dao.create(wine);
	}

	@PUT @Path("{id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Wine update(Wine wine) {
		System.out.println("Updating wine: " + wine.getName());
		dao.update(wine);
		return wine;
	}
	
	@DELETE @Path("{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public void remove(@PathParam("id") int id) {
		dao.remove(id);
	}

}
