package org.coenraets.cellar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class ConnectionHelper
{
	private String url;
	private static ConnectionHelper instance;

	private ConnectionHelper()
	{
    	String driver = null;
		try {
//			Class.forName("com.mysql.jdbc.Driver");
			Class.forName("org.postgresql.Driver");
			url = "jdbc:postgres://7G9HBS1/cellar?user=cellar&password=Chep1234";
            ResourceBundle bundle = ResourceBundle.getBundle("cellar");
            driver = bundle.getString("jdbc.driver");
            Class.forName(driver);
            url=bundle.getString("jdbc.url");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Connection getConnection() throws SQLException {
		if (instance == null) {
			instance = new ConnectionHelper();
		}
		try {
			System.out.println("CST:"+instance.url);
			Connection connection = DriverManager.getConnection(instance.url);
			
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery("Select * from wine");
            while(rs.next()){
                System.out.println(rs.getString(1));
            }
			
			return connection;
		} catch (SQLException e) {
			throw e;
		}
	}
	
	public static void close(Connection connection)
	{
		try {
			if (connection != null) {
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}